export const state = () => ({

    menu: [
        {
            title: "Shrimp Tacos",
            recipe: "Il faut manger Gras et salé",
            ingredients: [{nom: "Saumon", quantite: "50"}, {nom: "Thon", quantite: "50"}, {
                nom: "Viande",
                quantite: "50"
            }],
            onMenu: true,
        },
        {
            title: "Burger Tacos",
            recipe: "Il faut manger Salouche et salé",
            ingredients: [{nom: "Saumon", quantite: "50"}],
            onMenu: false,
        },
        {
            title: "Test Tacos",
            recipe: "Il faut manger sucré et Salva",
            ingredients: [{nom: "Saumon", quantite: "50"}],
            onMenu: false,
        },
        {
            title: "Pizza Tacos",
            recipe: "Il faut manger salut et salé",
            ingredients: [{nom: "Saumon", quantite: "50"}],
            onMenu: true,
        },
    ],
});

export const mutations = {


};
