// webpack.config.js
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const nodeExternals = require('webpack-node-externals')

module.exports = {
    module: {
        rules: [
            // ... other rules
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            }
        ]
    },
    externals: [nodeExternals()],
    plugins: [
        // make sure to include the plugin!
        new VueLoaderPlugin(),
    ]
}