import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import Vue from 'vue';
import App from './App.vue';
import './main.html';
import './main.styl';

import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
Vue.use(Vuetify); // this is all you need for Vuetify 1.x
import '/client/shoppingList/ShoppingList'

const vuetify = new Vuetify({});

Meteor.startup(() => {
  new Vue({
    el: '#app',
    ...App,
    vuetify,
  });
});

Template.hello.onCreated(function helloOnCreated() {
  // counter starts at 0
  this.counter = new ReactiveVar(0);
});

Template.hello.helpers({
  counter() {
    return Template.instance().counter.get();
  },
});

Template.hello.events({
  'click button'(event, instance) {
    // increment the counter when button is clicked
    instance.counter.set(instance.counter.get() + 1);
  },
});
